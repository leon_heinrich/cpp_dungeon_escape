# Dungeon Escape Game

The user have to find the exit (represented by "2") by walking through the labyrinth. 
The problem is that just a window of 3x3 fields is shown.

![](https://gitlab.com/leon_heinrich/cpp_dungeon_escape/-/raw/master/images/img_1.jpg)

![](https://gitlab.com/leon_heinrich/cpp_dungeon_escape/-/raw/master/images/img_2.jpg)

If the user reaches the exit the game is finished.

