#include <iostream>
#include "./header_files/Field.hpp"
#pragma once
#include <conio.h>

int main() {
    std::cout << "WELCOME TO DUNGEON ESCAPE!" << std::endl;
    std::cout << "Here is your starting position:" << std::endl;
    Field f;
    f.showPlayerView();
    std::cout << "Instructions: 0-Path, 1-Walls, X-Actual position" << std::endl;
    while (true) {
        int actualX = f.getPlayerPosition().getX();
        int actualY = f.getPlayerPosition().getY();
        std::cout << "Chose a direction where you want to go! Use W/A/S/D to move..." << std::endl;

        //Instantly get user-input without ENTER by using getch() 
        switch (getch()) {
            case 'W': case 'w':
                if(f.checkUp(actualX, actualY)){
                    f.setPlayerPosition(actualX, actualY-1);
                }
                break;
            case 'A': case 'a':
                if(f.checkLeft(actualX, actualY)){
                    f.setPlayerPosition(actualX-1, actualY);
                }
                break;
            case 'S': case 's':
                if(f.checkBot(actualX, actualY)){
                    f.setPlayerPosition(actualX, actualY+1);
                }
                break;
            case 'D': case 'd':
                if(f.checkRight(actualX, actualY)){
                    f.setPlayerPosition(actualX+1, actualY);
                }
                break;
            default:
                std::cout << "That was the wrong input! Try again..." << std::endl;
                break;
        }
        if(f.finished(f.getPlayerPosition().getX(), f.getPlayerPosition().getY())) {
            std::cout << "You reached the goal! Congratulations!" << std::endl;
            return 0;
        }
        f.showPlayerView();
    }
}