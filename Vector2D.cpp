//
// Created by Leon on 14.11.2019.
//
#pragma once
#include "./header_files/Vector2D.hpp"

Vector2D::Vector2D() = default;

Vector2D::Vector2D(int x, int y) {
    this -> xVector = x;
    this -> yVector = y;
}

int Vector2D::getX() {
    int xCopy = xVector;
    return xCopy;
}

int Vector2D::getY() {
    int yCopy = yVector;
    return yCopy;
}

void Vector2D::setXY(int x, int y) {
    this -> xVector = x;
    this -> yVector = y;
}




