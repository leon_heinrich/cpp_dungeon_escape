#pragma once
#include <string>
#include <iostream>
#include "./header_files/Field.hpp"

Field::Field() {
    map = {
            {1, 1, 1, 1, 1, 1},
            {1, 0, 0, 0, 0, 1},
            {1, 0, 1, 1, 0, 1},
            {1, 0, 0, 1, 1, 1},
            {1, 0, 0, 0, 0, 2},
            {1, 1, 1, 1, 1, 1},
    };
    playerPosition = Vector2D(4,2);
}

Vector2D Field::getPlayerPosition() {
    return playerPosition;
}

void Field::showPlayerView() {
    int x = playerPosition.getX();
    int y = playerPosition.getY();
    for(int i = y - 1; i <= y + 1; i++) {
        for(int j = x - 1; j <= x + 1; j++) {
            if(j == x && i == y) {
                std::cout << "X ";
            } else {
                std::cout << map[i][j] << " ";
            }
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void Field::setPlayerPosition(int x, int y) {
    playerPosition.setXY(x,y);
}

bool Field::checkUp(int actualX, int actualY) {
    if (map[actualY-1][actualX] == 1) {
        Field::wallFail();
    } else {
        return true;
    }
}

bool Field::checkBot(int actualX, int actualY) {
    if (map[actualY+1][actualX] == 1) {
        Field::wallFail();
    } else {
        return true;
    }
}

bool Field::checkLeft(int actualX, int actualY) {
    if (map[actualY][actualX-1] == 1) {
        Field::wallFail();
    } else {
        return true;
    }
}

bool Field::checkRight(int actualX, int actualY) {
    if (map[actualY][actualX+1] == 1) {
        Field::wallFail();
    } else {
        return true;
    }
}

bool Field::wallFail() {
    std::cout << "In this direction is a wall! Chose another direction!" << std::endl;
    return false;
}

bool Field::finished(int actualX, int actualY) {
    if(map[actualY][actualX] == 2) {
        return true;
    }
}






