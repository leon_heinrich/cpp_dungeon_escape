//
// Created by Leon on 14.11.2019.
//

#pragma once


class Vector2D {

    int xVector, yVector;

    public:
        Vector2D();
        Vector2D(int x, int y);

    public:
        int getX();
        int getY();
        void setXY(int x, int y);
};


