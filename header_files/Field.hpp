//
// Created by Leon on 14.11.2019.
//

#pragma once

#include <vector>
#include "Vector2D.hpp"

class Field {

    std::vector<std::vector<int>> map;
    Vector2D playerPosition;

public:
    Field();

public:
    Vector2D getPlayerPosition();

public:
    void setPlayerPosition(int x, int y);

public:
    void showPlayerView();

public:
    bool checkUp(int actualX, int actualY);
    bool checkBot(int actualX, int actualY);
    bool checkLeft(int actualX, int actualY);
    bool checkRight(int actualX, int actualY);
    static bool wallFail();
    bool finished(int actualX, int actualY);
};



